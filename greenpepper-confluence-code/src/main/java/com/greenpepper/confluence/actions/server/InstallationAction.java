package com.greenpepper.confluence.actions.server;

import com.atlassian.confluence.velocity.htmlsafe.HtmlSafe;
import com.greenpepper.confluence.GreenPepperServerConfigurationActivator;
import com.greenpepper.server.GreenPepperServerException;
import com.greenpepper.server.configuration.DatabaseConfiguration;
import com.greenpepper.server.database.SupportedDatabases;
import com.greenpepper.util.I18nUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * <p>InstallationAction class.</p>
 *
 * @author oaouattara
 * @version $Id: $Id
 */
@SuppressWarnings("serial")
public class InstallationAction
		extends GreenPepperServerAction
{

	private static final Logger log = LoggerFactory.getLogger(InstallationAction.class);
	
	private static final String RESOURCE_BUNDLE = InstallationAction.class.getName();
	public static final String CUSTOM_INSTALL = "customInstall";
	public static final String QUICK_INSTALL = "quickInstall";
	private final ThreadLocal<Locale> threadLocale = new ThreadLocal<Locale>();
	private ResourceBundle resourceBundle;

	private String installType;

	private DatabaseConfiguration databaseConfiguration;
	private String databaseType;
	private String username;
	private String password;
	private String host;
	private String port;
	private String database;


	private boolean editMode;

	
	/** {@inheritDoc} */
	@Override
	public String getActionName(String fullClassName) {
		return getText("greenpepper.install.title");
	}

	/**
	 * <p>Constructor for InstallationAction.</p>
	 */
	public InstallationAction()
	{
	}

	/**
	 * <p>config.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String config() {
		return SUCCESS;
	}

	/**
	 * <p>getDialects.</p>
	 *
	 * @return a {@link java.util.List} object.
	 */
	public List<SupportedDatabases> getDatabases()
	{
		return Arrays.asList(SupportedDatabases.values());
	}

	//We want to force edit mode if DBMS not ready 
	/**
	 * <p>Getter for the field <code>editMode</code>.</p>
	 *
	 * @return a {@link java.lang.Boolean} object.
	 */
	public Boolean getEditMode() {
		return editMode | !(isServerReady());
	}

	/**
	 * <p>Setter for the field <code>editMode</code>.</p>
	 *
	 * @param editMode a {@link java.lang.Boolean} object.
	 */
	public void setEditMode(Boolean editMode) {
		this.editMode = editMode;
	}

	/**
	 * <p>getIsCustomSetup.</p>
	 *
	 * @return a boolean.
	 */
	public boolean getIsCustomSetup()
	{
		return isCustomSetup();
	}

	/**
	 * <p>isCustomSetup.</p>
	 *
	 * @return a boolean.
	 */
	public boolean isCustomSetup()
	{
		return getInstallType().equals(CUSTOM_INSTALL);
	}

	/**
	 * <p>Getter for the field <code>installType</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getInstallType()
	{
		return  installType == null ? (getDatabaseType() == null ? QUICK_INSTALL : CUSTOM_INSTALL) : installType;
	}

	/**
	 * <p>Setter for the field <code>installType</code>.</p>
	 *
	 * @param installType a {@link java.lang.String} object.
	 */
	public void setInstallType(String installType)
	{
		this.installType = installType;
	}

	/**
	 * <p>changeInstallationType.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String changeInstallationType()
	{
		return SUCCESS;
	}

	public String getDatabaseType() {
		return databaseType == null ? getConfigurationActivator().getConfigDbType() : databaseType;
	}

	public void setDatabaseType(String databaseType) {
		this.databaseType = databaseType;
		// Reinit the database configuration
		this.databaseConfiguration = getDatabaseConfigurationFromString(databaseType);
	}

	private DatabaseConfiguration getDatabaseConfiguration() {
		if (this.databaseConfiguration == null && isNotBlank(getDatabaseType())) {
			this.databaseConfiguration = getDatabaseConfigurationFromString(getDatabaseType());
			assert databaseConfiguration != null;
			databaseConfiguration.setDatabase(getDatabase());
			databaseConfiguration.setHost(getHost());
			databaseConfiguration.setPort(getPort());
			databaseConfiguration.setUsername(getUsername());
			databaseConfiguration.setPassword(getPassword());
		}
		return databaseConfiguration;
	}


	private DatabaseConfiguration getDatabaseConfigurationFromString(String databaseType) {
		if (isBlank(databaseType))
			return null;
		SupportedDatabases database = SupportedDatabases.valueOf(databaseType);
		try {
			return database.getDbConfigClass().newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			throw new IllegalStateException("Problem determining the database type: " + databaseType, e);
		}
	}

	public String getUsername() {
		return username == null ? getConfigurationActivator().getConfigDbUsername() : username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password == null ? getConfigurationActivator().getConfigDbPassword() : password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getHost() {
		return host == null ? getConfigurationActivator().getConfigDbHost() : host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getPort() {
		return port == null ? getConfigurationActivator().getConfigDbPort() : port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getDatabase() {
		return database == null ? getConfigurationActivator().getConfigDbname() : database;
	}

	public void setDatabase(String database) {
		this.database = database;
	}

	/**
	 * {@inheritDoc}
	 *
	 * Custom I18n. Based on WebWork i18n.
	 */
	@HtmlSafe
	public String getText(String key)
	{
		String text = super.getText(key);

		if (text.equals(key))
		{
			text = I18nUtil.getText(key, getResourceBundle());
		}

		return text;
	}

	private ResourceBundle getResourceBundle() {

		if (resourceBundle == null)
		{
			Locale locale = threadLocale.get();
			if (locale == null)
			{
				locale = getLocale();
				threadLocale.set(locale == null ? Locale.ENGLISH : locale);
			}

			resourceBundle = ResourceBundle.getBundle(RESOURCE_BUNDLE, locale);
		}

		return resourceBundle;
	}

	/**
	 * <p>isSetupComplete.</p>
	 *
	 * @return a boolean.
	 */
	public boolean isSetupComplete()
	{
		return gpUtil.isServerSetupComplete();
	}

	/**
	 * <p>isServerReady.</p>
	 *
	 * @return a boolean.
	 */
	public boolean isServerReady()
	{
		return gpUtil.isServerReady();
	}

	/**
	 * <p>editDbmsConfiguration.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String editDbmsConfiguration()
	{
		try
		{
			if (isCustomSetup())
			{
				if (getDatabaseConfiguration() != null && isNotBlank(database))
				{
					if (canConnectToDbms())
					{
						updateDatabaseConfiguration();
						getConfigurationActivator().initCustomInstallConfiguration(getDatabaseConfiguration(), databaseType);
					}
					else
					{
						addActionError("greenpepper.install.dbms.test.failure");
					}
				}
				else
				{
					addActionError("greenpepper.install.dbms.test.failure");
				}
			}
			else
			{
				getConfigurationActivator().initQuickInstallConfiguration();
			}
		}
		catch (GreenPepperServerException ex)
		{
			addActionError("greenpepper.install.dbms.init.failure");
		}

		return SUCCESS;
	}

	private void updateDatabaseConfiguration() {
		getDatabaseConfiguration().setDatabase(database);
		getDatabaseConfiguration().setHost(host);
		getDatabaseConfiguration().setPassword(password);
		getDatabaseConfiguration().setUsername(username);
		getDatabaseConfiguration().setPort(port);
	}

	/**
	 * <p>testDbmsConnection.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String testDbmsConnection()
	{
		LOG.info("Testing the Database configuration");
		try {
			testDatabaseConnection();
		} catch (Exception e) {
			addActionError(e.getMessage());
		}

		return SUCCESS;
	}

	private boolean canConnectToDbms()
	{
		try
		{
			testDatabaseConnection();
			return true;
		}
		catch (Exception ex)
		{
			log.error("Testing Dbms Connection using JDBC (" + getDatabaseConfiguration().getJDBCUrl() + ")", ex);
			return false;
		}
	}

	private void testDatabaseConnection() throws ClassNotFoundException, SQLException {
		updateDatabaseConfiguration();
		Class.forName(getDatabaseConfiguration().getDriverClass());
		Connection connection = DriverManager.getConnection(getDatabaseConfiguration().getJDBCUrl(), username, password);
		connection.close();
	}

	private GreenPepperServerConfigurationActivator getConfigurationActivator() {
		return gpUtil.getGPServerConfigurationActivator();
	}
}
