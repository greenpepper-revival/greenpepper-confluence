# HOW TO TEST ?


> **NOTE:** Confluence 5.0.3
>
> This version of confluence requires JDK 6. 

## Setup your environment 

* [Atlassian doc](https://developer.atlassian.com/docs/getting-started/set-up-the-atlassian-plugin-sdk-and-build-a-project)

### JDK 7

```bash
wget --no-check-certificate --no-cookies --header "Cookie: oraclelicense=accept-securebackup-cookie" \
     http://download.oracle.com/otn-pub/java/jdk/7u80-b15/jdk-7u80-linux-x64.tar.gz
```

```bash
export JAVA_HOME=~/installs/jdk1.7.0_80/
export M2_HOME=~/installs/apache-maven-3.5.2/
```

### JDK 6

```bash
export JAVA_HOME=~/installs/jdk1.6.0_45/
export M2_HOME=~/installs/apache-maven-3.2.5/
```


## Commands to launch

```bash
atlas-run
```

OR

```bash
export ATLAS_HOME=$(dirname $(dirname $(realpath /usr/bin/atlas-run)))
mvn amps:run-standalone
```